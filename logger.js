const { createLogger, format, transports } = require('winston');
const path = require('path');

const formats = format.combine(
    format.colorize(),
    format.timestamp(),
    format.simple(),
);

const logger = createLogger({
    transports: [
      new transports.File({
        filename: path.join(__dirname,'logs/combined.log'),
        level: 'info',
        format: formats
      }),
      new transports.File({
        filename: path.join(__dirname,'logs/error.log'),
        level: 'error',
        format: formats
      })
    ]
});

module.exports = logger;