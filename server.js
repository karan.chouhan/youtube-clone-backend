const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const AWS = require('aws-sdk');
const cors = require('cors');
const env = require('./config');
const { v4: uuidv4 } = require('uuid');
const logger = require('./logger');
const port = process.env.PORT || 8080;


const app = express();
app.use(cors());
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

require('./app/router/router.js')(app);

const db = require('./app/db.config.js');

// force: true will drop the table if it already exists
db.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', { raw: true }).then(() => {
    db.sequelize.sync({ force: true }).then(() => {
        console.log('Drop and Resync with { force: true }');
    })
});


//!AWS
const s3 = new AWS.S3({
    accessKeyId: env.aws_access,
    secretAccessKey: env.aws_secret
})


const storage = multer.memoryStorage({
    destination: function (req, file, callback) {
        callback(null, '');
    }
})

const uploadVideo = multer({ storage: storage }).single('video');

const uploadImg = multer({ storage: storage }).single('img');


//! upload video
app.post('/upload/video', uploadVideo, (req, res) => {

    console.log(req.file.originalname);
    let myFile = req.file.originalname.split(".");
    const filetype = myFile[myFile.length - 1]

    const params = {
        Bucket: 'youtube-clone23',
        Key: `${uuidv4()}.${filetype}`,
        Body: req.file.buffer,
        ACL: 'public-read'
    }

    s3.upload(params, (error, data) => {
        if (error) {
            logger.info('Video is uploaded to AWS S3 successfully');
            res.status(500).send(error);
        } else {
            logger.error('Something went wrong when uploading video to AWS S3');
            res.send(data.Location);
        }

    })
})

//! upload img
app.post('/upload/img', uploadImg, (req, res) => {

    let myFile = req.file.originalname.split(".");
    const filetype = myFile[myFile.length - 1]

    const params = {
        Bucket: 'youtube-clone23',
        Key: `${uuidv4()}.${filetype}`,
        Body: req.file.buffer,
        ACL: 'public-read'
    }

    s3.upload(params, (error, data) => {
        if (error) {
            logger.info('Image is uploaded to AWS S3 successfully');
            res.status(500).send(error)
        } else {
            logger.error('Something went wrong when uploading image to AWS S3');
            res.send(data.Location)
        }
    })
})

const server = app.listen(port, function () {
    const host = server.address().address
    const port = server.address().port
    logger.info(`App listening at port ${port}`, port)
})
