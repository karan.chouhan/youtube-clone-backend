module.exports = (sequelize, Sequelize) => {
    const Comment = sequelize.define('comments', {
        
        videoId: {
            type: Sequelize.UUID,
            references: {
                model: 'videos',
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.UUID,
            references: {         
                model: 'users',
                key: 'id'
            }
        },
        commentText: {
            type: Sequelize.TEXT,
            allowNull: false
        }
        
    });

    return Comment;
}