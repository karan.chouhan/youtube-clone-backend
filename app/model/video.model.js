module.exports = (sequelize, Sequelize) => {
    const Video = sequelize.define('videos', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false
        },
        channelId: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'channels',
                key: 'id'
            }
        },
        likes: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        views: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        category: {
            type: Sequelize.STRING,
            allowNull: false
        },
        unLikes: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return Video;
}


// - video- id,channelID,no. of likes,views,length,description,title, added at,
// dataApi for video,no, of unlikes,categoryId