module.exports = (sequelize, Sequelize) => {
    const video_storage = sequelize.define('video_storage', {
        videoId: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'videos',
                key: 'id'
            }
        },
        url: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return video_storage;
}
