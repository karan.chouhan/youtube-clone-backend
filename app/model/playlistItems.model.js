module.exports = (sequelize, Sequelize) => {
    const PlaylistItems = sequelize.define('playlistItems', {


        playlistId: {
            type: Sequelize.UUID,
            references: {         
                model: 'playlists',
                key: 'id'
            }
        },
        videoId: {
            type: Sequelize.UUID,
            references: {
                model: 'videos',
                key: 'id'
            }
        }

    });

    return PlaylistItems;
}