module.exports = (sequelize, Sequelize) => {
    const Playlist = sequelize.define('playlists', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false
        },
        name : {
            type: Sequelize.STRING
        },
        userId: {
            type: Sequelize.UUID,
            references: {
                model: 'users',
                key: 'id'
            }
        }
    });

    return Playlist;
}