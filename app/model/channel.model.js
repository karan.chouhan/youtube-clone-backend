

module.exports = (sequelize, Sequelize) => {
    const Channel = sequelize.define('channels', {
        id: {
            type: Sequelize.UUID,
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        numOfSubscribers: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        userId: {
            type: Sequelize.UUID,
            references: {         // WorkingDays hasMany Users n:n
                model: 'users',
                key: 'id'
            }
        }
    });
    return Channel;
}

// name,id,email,no. of subscribers,UserId