module.exports = (sequelize, Sequelize) => {
    const Subscription = sequelize.define('subscription', {
        userId: {
            type: Sequelize.UUID,
            references: {         // WorkingDays hasMany Users n:n
                model: 'users',
                key: 'id'
            }
        },
        channelId: {
            type: Sequelize.UUID,
            references: {
                model: 'channels',
                key: 'id'
            }
        }
    });

    return Subscription;
}