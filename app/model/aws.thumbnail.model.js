module.exports = (sequelize, Sequelize) => {
    const Thumbnail_storage = sequelize.define('thumbnail_storage', {
        videoId: {
            type: Sequelize.UUID,
            allowNull: false,
            references: {
                model: 'videos',
                key: 'id'
            }
        },
        url: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return Thumbnail_storage;
}