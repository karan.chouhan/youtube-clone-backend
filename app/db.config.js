const env = require('../config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.user, env.password, {
  host: env.host,
  dialect: 'mysql',
  operatorsAliases: false,
  pool: {
    max: 50,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('./model/user.model.js')(sequelize, Sequelize);
db.role = require('./model/role.model.js')(sequelize, Sequelize);
db.channel = require('./model/channel.model')(sequelize, Sequelize);
db.subscription = require('./model/subscription.model')(sequelize, Sequelize);
db.video_storage = require('./model/aws.video.model')(sequelize, Sequelize);
db.video = require('./model/video.model')(sequelize, Sequelize);
db.thumbnail_storage = require('./model/aws.thumbnail.model')(sequelize, Sequelize);
db.playlist = require('./model/playlist.model')(sequelize, Sequelize);
db.playlistItem = require('./model/playlistItems.model')(sequelize, Sequelize);
db.comment = require('./model/comment.model')(sequelize, Sequelize);

db.user.hasOne(db.channel, { foreignKey: 'userId' });

db.subscription.belongsTo(db.user, { foreignKey: 'userlId' });
db.channel.hasMany(db.subscription, { foreignKey: 'channelId' });
db.subscription.belongsTo(db.channel, { foreignKey: 'channelId' })

db.channel.hasMany(db.video, { foreignKey: 'channelId' });
db.video.belongsTo(db.channel, { foreignKey: 'channelId' })

db.video.hasOne(db.video_storage, { foreignKey: 'videoId' });

db.video.hasOne(db.thumbnail_storage, { foreignKey: 'videoId' });

//! playlist -> channel can have many playlist
db.playlist.belongsTo(db.user, { foreignKey: 'userId' });

//! playlistItems
db.playlistItem.belongsTo(db.video, { foreignKey: 'videoId' });
db.playlist.hasMany(db.playlistItem, { foreignKey: 'playlistId' });


//! comments
db.video.hasMany(db.comment, { foreignKey: 'videoId' });
db.comment.belongsTo(db.user, { foreignKey: 'userId' });

db.role.belongsToMany(db.user, { through: 'user_roles', foreignKey: 'roleId', otherKey: 'userId' });
db.user.belongsToMany(db.role, { through: 'user_roles', foreignKey: 'userId', otherKey: 'roleId' });
module.exports = db;