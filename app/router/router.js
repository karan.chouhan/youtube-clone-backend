const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');
const Sequelize = require('sequelize');
const db = require('../db.config');
const { v4: uuidv4 } = require('uuid');
const logger = require('../../logger');

module.exports = function (app) {

	const controller = require('../controller/controller.js');

	app.post('/api/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail], controller.signup);

	app.post('/api/auth/signin', controller.signin);

	app.get('/api/test/user', [authJwt.verifyToken], controller.userContent);

	app.get('/api/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], controller.managementBoard);

	app.get('/api/test/admin', [authJwt.verifyToken, authJwt.isAdmin], controller.adminBoard);


	app.post('/subscribe', (req, res) => {
		db.subscription.create({
			userId: req.body.userId,
			channelId: req.body.channelId
		}).then((data) => {
			logger.info('Added subscriber successfully');
			res.send(data);
		}).catch((err) => {
			logger.error('Adding subscriber failed');
			res.send(err);
		})
	})

	app.get('/subscriptions/:userid', [authJwt.verifyToken], (req, res) => {
		db.subscription.findAll({
			where: {
				userId: req.params.userid
			},
			include: [{
				model: db.channel
			}]
		}).then((data) => {
			logger.info('Subscribers are sent successfully');
			res.send(data);
		}).catch((err) => {
			logger.error('Subscribers are sent successfully');
			res.send(err);
		})
	})

	// (this)
	app.post('/createplaylist/:userid', [authJwt.verifyToken], (req, res) => {
		db.playlist.create({
			id: uuidv4(),
			userId: req.params.userid,
			name: req.body.name,
		}).then((data) => {
			logger.info('Created playlist successfully');
			res.send(data);
		}).catch((err) => {
			logger.error('Playlist creation failed');
			res.send(err);
		})
	})

	// (this)
	app.post('/addvideo/:channelid', [authJwt.verifyToken], (req, res) => {
		db.video.create({
			id: uuidv4(),
			title: req.body.title,
			channelId: req.params.channelid,
			unLikes: 0,
			likes: 0,
			description: req.body.description,
			category: req.body.category,
			views: 0
		}).then((data) => {
			db.video_storage.create({
				videoId: data.id,
				url: req.body.videoUrl
			}).then(data => {
				db.thumbnail_storage.create({
					videoId: data.videoId,
					url: req.body.imgUrl
				})
			})
		}).then(data => {
			logger.info('Video added successfully');
			res.send(data);
		}).catch((err) => {
			logger.error('Adding video failed');
			res.send(err);
		})
	})

	// (this)
	app.post('/addtoplaylist/:playlistid', [authJwt.verifyToken], (req, res) => {

		db.playlistItem.create({
			playlistId: req.params.playlistid,
			videoId: req.body.videoId
		}).then((data) => {
			logger.info('Video added to playlist');
			res.send(data);
		}).catch((err) => {
			logger.error('Adding video to playlist failed');
			res.send(err);
		})
	})

	// (this)
	app.get('/playlists/:userid', [authJwt.verifyToken], (req, res) => {
		db.playlist.findAll({
			where: { userId: req.params.userid },
			include: [{
				model: db.playlistItem,
				include: [{
					model: db.video
				}]
			}]
		})
			.then((data) => {
				res.send(data)
			}).catch(err => {
				res.send(err)
			})
	})

	app.get('/playlist/:playlistid', [authJwt.verifyToken], (req, res) => {
		db.playlist.findAll({
			where: { id: req.params.playlistid },
			include: [{
				model: db.playlistItem,
				include: [{
					model: db.video,
					include: [{
						model: db.thumbnail_storage
					}]
				}]
			}]
		}).then(data => {
			res.send(data)
		}).catch(err => {
			res.send(err)
		})
	})

	// (this)
	app.post('/comment/:videoid', [authJwt.verifyToken], (req, res) => {
		db.comment.create({
			userId: req.body.userId,
			videoId: req.params.videoid,
			commentText: req.body.comment
		}).then((data) => {
			res.send(data)
		}).catch(err => {
			res.send(err)
		})
	})

	app.post('/like/:videoid', (req, res) => {
		db.video.increment(
			{ views: +1 },
			{ where: { id: req.params.videoid } }
		).then((data) => {
			logger.info('Add likes to video');
			res.send(data);
		}).catch((err) => {
			logger.error('Adding likes failed');
			res.send(err);
		})
	})

	// (this)
	app.post('/subscribe/:channelid', [authJwt.verifyToken], (req, res) => {
		db.subscription.create({
			userId: req.body.userId,
			channelId: req.params.channelid
		}).then(data => {
			logger.info('Subscribing to channel is successful');
			res.send(data);
		}).catch(err => {
			logger.error('Subscribing to channel failed');
			res.send(err);
		})
	})

	// (this)
	app.post('/unsubscribe/:channelid', [authJwt.verifyToken], (req, res) => {
		db.subscription.destroy({
			where: {
				userId: req.body.userId,
				channelId: req.params.channelid
			}
		}).then((data) => {
			logger.info('Subscribing to channel is successful');
			res.sendStatus(200).send(data);
		}).catch((err) => {
			logger.error('Subscribing to channel is successful');
			res.send(err);
		})
	})

	app.get('/video/:videoid', (req, res) => {
		db.video.findAll({
			where: { id: req.params.videoid },
			include: [
				{
					model: db.comment,
					include: [{
						model: db.user
					}]
				},
				{ model: db.video_storage },
				{
					model: db.channel,
					include: [{
						model: db.subscription
					}]
				}
			]
		}).then((data) => {
			logger.info('All videos sent successfully');
			res.send(data);
		}).catch((err) => {
			logger.error('Sending videos failed');
			res.send(err);
		})
	})

	app.get('/mychannel/:userid', (req, res) => {
		db.channel.findAll({
			where: {
				userId: req.params.userid
			}
		}).then((data) => {
			logger.info('Sending users id is successful');
			res.send(data);
		}).catch((err) => {
			logger.error('Sending users id failed');
			res.send(err);
		})
	})

	app.get('/allvideos', (req, res) => {
		db.video.findAll({
			include: [{
				model: db.thumbnail_storage
			}, { model: db.channel }]
		})
			.then((data) => {
				logger.info('All videos sent successfully');
				res.send(data);
			}).catch((err) => {
				logger.error('sending videos failed');
				res.send(err)
			})
	})

	app.get('/channelvideos/:channelid', (req, res) => {
		db.video.findAll({
			where: { channelId: req.params.channelid },
			include: [{
				model: db.thumbnail_storage
			}, { model: db.channel }]
		}).then((data) => {
			logger.info('request success /channelvideos/:channelid');
			res.send(data);
		}).catch((err) => {
			logger.error('request unsuccessful /channelvideos/:channelid');
			res.send(err);
		})
	})

	app.get('/recommend/:category', (req, res) => {
		db.video.findAll({
			where: { category: req.params.category },
			include: [{
				model: db.thumbnail_storage
			}, {
				model: db.channel
			}]
		}).then((data) => {
			logger.info('Successfully send recommendations');
			res.send(data);
		}).catch((err) => {
			logger.error('Sending recommendations failed');
			res.send(err);
		})
	})
	//this
	app.delete('/deletevideo/:id', [authJwt.verifyToken], (req, res) => {
		db.video.destroy({
			where: {
				id: req.params.id
			}
		}).then(data => {
			logger.info('Video deleted successfully');
			res.sendStatus(200).send(data);
		}).catch(error => res.send(error))
	})
}